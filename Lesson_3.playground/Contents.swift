//#1
let array = [
    12, 34, -5, 3,
    13, 63, 33, 7
]
let arraySortedUp = array.sorted(by: {$0 < $1})
let arraySortedDown = array.sorted(by: {$1 < $0})

print(arraySortedUp)
print(arraySortedDown)

//#2
var namesOfFriends = ["Kostya", "Danik", "Klim", "Seroov"]
func filteredNames (array: [String]) -> [String] {
    var filteredNames = namesOfFriends.sorted(by: {$0.count < $1.count})
    return (filteredNames)
}
filteredNames(array: namesOfFriends)

//Создать словарь (Dictionary), где ключ - кол-во символов в имни, а в значении - имя друга

var dict = [Int : String]()

for element in namesOfFriends {
    dict[element.count] = element
}

print(dict)

//▸ Написать функцию, которая будет принимать ключ, выводить полученный ключ и значение
func keysAndValues(key:Int)  {
    print("Key:\(key) , Value:\(dict[key] ?? "такого нет")")
}
keysAndValues(key: 10)
keysAndValues(key: 4)

//Написать функцию, которая принимает 2 массива (один строковый, второй - числовой) и проверяет их на пустоту: если пустой - то добавьте любое значения и выведите массив в консоль
var stringArray = [String]()
var intArray = [Int]()

func addIfEmpty(arr1: inout [String], arr2: inout [Int]) {
    
    if arr1.isEmpty{
        arr1.append("Hi")
    }
    
    if arr2.isEmpty{
        arr2.append(345)
    }
}

addIfEmpty(arr1: &stringArray, arr2: &intArray)

stringArray
intArray
